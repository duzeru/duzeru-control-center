const remote = require('electron').remote
const main = remote.require('./mainElectron.js');
const shell = require('shelljs')


$(document).ready(function () {

    $('#search').focus();
    btnClear();


    /*input search*/
    $('#search').keyup(function () {
        var nomeFiltro = $(this).val().toLowerCase();
        $('.icons').find('div').each(function () {
            var corresponde = $(this).text().toLowerCase().indexOf(nomeFiltro) >= 0;
            $(this).parent().parent().css('display', corresponde ? '' : 'none');
        });
    });


    setTimeout(function () {
        inf.kernel();
        inf.user();
        inf.architecture();
        inf.versionXFCE();
    }, 200);

    inf.getMemory();
    inf.getOS();
    inf.getHD();
    inf.getSound();
    inf.getGraphics();
    inf.getCPU();

});
var inf = (function () {

    function architecture() {
        shell.exec("arch", (a, b, c) => {
            $('#pnArchi').html(b);
        });
    }

    function user() {
        shell.exec("who | awk '{ print $1 }' | head -n 1", (a, b, c) => {
            $('#pnUser').html(b.toLowerCase().replace(/(?:^|\s)\S/g, function (a) {
                return a.toUpperCase();
            }));
        });
    }

    function kernel() {
        shell.exec("uname -rs", (a, b, c) => {
            $('#pnKernel').html(b);
        });
    }

    function versionXFCE() {
        shell.exec("xfce4-session -V | grep xfce4-session", (a, b, c) => {
            $('#pnXfceVersion').html(b);
        });
    }

    function getOnlyNumber(any)
    {
        let numberSplit = any.split('.');

        let numberToString = ''

        for(i = 0; i < numberSplit.length; i++)
        {
        
            numberToString += numberSplit[i].replace(/[^0-9]/g, '');

            numberToString = numberToString + '.';

        }

        return parseFloat(numberToString);
    }

    function getMemory() {
        shell.exec("free -h | grep Mem | awk '{print $2, $3}'", (a, b, c) => {
            var menTotalConvert = false;
            var menUsedConvert = false;

            if (b.split(' ')[0].indexOf('M') > 0)
                menTotalConvert = true;

            if (b.split(' ')[1].indexOf('M') > 0)
                menUsedConvert = true;

            var men = b.split(' ')[0];
            b = b.replace(/\G/g, '').replace(/\M/g, '').replace(/\T/g, '');
            var valor = b.replace(/\./g, '').replace(/\,/g, '.').split(' ');

            if (menTotalConvert)
                valor[0] = parseFloat(valor[0]) / 1024;

            if (menUsedConvert)
                valor[1] = parseFloat(valor[1]) / 1024;

            var porcento = ((getOnlyNumber (valor[1]) / getOnlyNumber (valor[0]) * 100)).toFixed(2);
            $('#pnPercentRam div').html(porcento + '%').css('width', porcento);
            $('#pnRAM').html(men);
            
        });
    }

    function getOS() {
        shell.exec("lsb_release -d | sed 's/Description:[\t]//g'", (a, b, c) => {
            $('#pnOS').html(b);
        });
    }

    function getHD() {
        shell.exec("df -h / | awk '{print $2, $5}' | tail -n 1", (a, b, c) => {
            $('#pnHD').html(b.split(' ')[0]);
            $('#pnPercent div').html(b.split(' ')[1]).css('width', b.split(' ')[1]);
        });
    }


    function getSound() {
        shell.exec('lspci | grep -i audio | cut -d ":" -f 3 | head -n 1', (a, b, c) => {
            $('#pnSound').html(b);
        });
    }

    function getGraphics() {
        shell.exec('lspci | grep VGA | cut -d ":" -f 3 | cut -d "(" -f 1', (a, b, c) => {
            $('#pnGraphics').html(b);
        });

    }

    function getCPU() {
        shell.exec('lscpu | grep Model | tail -n 1 | cut -d ":" -f 2', (a, x, c) => {
            shell.exec('grep -c cpu[0-9] /proc/stat', (a, b, c) => {
                $('#pnCPU').html(x + 'x' + b);
            });
        });
    }

    return {
        architecture: architecture,
        user: user,
        kernel: kernel,
        versionXFCE: versionXFCE,
        getMemory: getMemory,
        getOS: getOS,
        getHD: getHD,
        getSound: getSound,
        getGraphics: getGraphics,
        getCPU: getCPU
    };
})();

var modalConf = (function () {
    var pnLocal;
    var advancedConfig;
    function modal() {
        $('#pnConf').mofo({
            noTitleDisplay: true,
            modal: false,
            fullScreen: true,
            buttons: {
                Btn: {
                    html: '<i class="fas fa-arrow-circle-left" aria-hidden="true"></i> ' + locate.back,
                    style: 'float:left',
                    click: function () {
                        $('#pnConf').mofo('close');
                    },
                },
                AdvanceConfig: {
                    html: '<i class="fas fa-cog" aria-hidden="true"></i> ' + locate.AdvancedConfig,
                    click: function () {
                        shell.exec(advancedConfig, () => {
                        });
                    },
                }
            },
            open: function () {
                $('#pnConf').find('.mofo-modal-content').html($('#' + pnLocal).html());

            },
            close: function () {
                $('.pnInf').show();
                $('#listIcon').remove();
                $('#pnListIcon').remove();
            }
        });
        $('#pnConf').css('z-index', '785');
    }

    $('.icons').click(function () {

        pnLocal = $(this).attr('pn');
        advancedConfig = $(this).attr('conf');

        var position = $(this).find('img').position();
        var html = $(this).find('div').html();
        position.width = $(this).find('img').attr('width');
        var src = $(this).find('img').attr('src');
        var img = $('<img>', {
            id: 'listIcon',
            css: position,
            src: src
        });
//        var div = $('<div>', {html: html, id: 'pnListIcon'});
//        $('.pnBarra').prepend(div).prepend(img);
//        $('.pnInf').hide();
//        setTimeout(function () {
//            $('#listIcon').css({
//                top: -5,
//                left: 10,
//                width: 65
//            });
//            modal();
//        }, 80);
    });
})();

function btnClear() {
    function tog(v) {
        return v ? 'addClass' : 'removeClass';
    }
    $(document).on('input', '.btnClear', function () {
        $(this)[tog(this.value)]('x');
    }).on('mousemove', '.x', function (e) {
        $(this)[tog(this.offsetWidth - 30 < e.clientX - this.getBoundingClientRect().left)]('onX');
    }).on('touchstart click', '.onX', function (ev) {
        ev.preventDefault();
        $(this).removeClass('x onX').val('').change();
        $('.icons').parent().css('display', '');
    });
}

function btnClick() {
    const rs = shell.cat('./package.json');
    console.log(rs.toString());
    //shell.echo(shell.ls('/home/alves').toString())

    shell.exec("find . -type f | xargs stat --format '%Y :%y %n' | sort -nr | cut -d: -f2- | head", function (code, stdout, stderr) {
        console.log('-------------');
        console.log(code);
        console.log('-------------');
        console.log(stdout);
        console.log('-------------');
        console.log(stderr);
    });
//
    shell.exec('nm-connection-editor', () => {
    });
}

$('#btn3').click(() => {

    shell.exec('xfconf-query -c xsettings -p /Net/IconThemeName -s "Tango"', (code, stdout, stderr) => {
        console.log(stdout);
    });
})

$('#btn').click(() => {
    btnClick()
});
